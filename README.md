# Python Neural Network

Projet basique de création d'un réseau de neurone deep learning avec rétropropagation, le fichier à analyser doit être appelé
"input-data.csv", être au format CSV comma separated.

Pour le moment le réseau ne fait "qu'apprendre", il faudrait une petite interface graphique pour régler le fichier input, quelques constantes comme le coef d'apprentissage par exemple, et pouvoir le passer en mode "Déduction" une fois l'apprentissage effectué.
