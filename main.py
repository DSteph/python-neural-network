import pandas as pd
import numpy as np
from Neurone import Neurone
from sklearn.preprocessing import MinMaxScaler

data = pd.read_csv("input-data.csv", ",")

# Outil de normalisation
scaler = MinMaxScaler()

# Normalisation des données, il faut qu'elles soient toutes numériques pour que ça fonctionne
normalised = pd.DataFrame(scaler.fit_transform(data), columns=data.columns, index=data.index)
normalised = normalised.round(2)
normalised.to_csv("./normalised.csv")
# On stock le nombre de colonnes - 1 puisque la dernière colonne sera celle à vérifier
nbcolonnes = normalised.columns.size - 1
# Potentiellement donner le choix a l'utilisateur
nblayer = 4
coef_appr = 0.01
# Pareil, règle à déterminer
nb_neurons_hidden_layer = nblayer + 1
network = []
for iterator1 in range(nblayer):
    # Première couche, nombre de neurones correspond au nombre d'entrées
    layer = []
    if iterator1 == 0:
        for iterator2 in range(nbcolonnes):
            layer.append(Neurone(nbcolonnes))
    elif iterator1 == nblayer - 1:
        layer.append(Neurone(nbcolonnes + 1))
    else:
        for iterator2 in range(nbcolonnes + 1):
            layer.append(Neurone(len(network[iterator1-1])))
    network.append(layer)

# Le nombre d'exemples sur lesquels on itère
learn_range = (len(normalised.index) * 90) // 100
# L'index à partir duquel on commencera l'étape de vérification, on garde 10%
# du fichier a la fin pour vérification
verify_index = learn_range + 1
training_complete = False
normalised_result = normalised.pop(normalised.columns[-1])

while not training_complete:
    correct_guess = 0
    for iterator1 in range(learn_range):
        wanted_value = normalised_result.loc[iterator1]
        # Forward propagation
        for index, layer in enumerate(network):
            if index == 0:
                for neurone in layer:
                    neurone.calc_sigma(normalised.iloc[iterator1])
            else:
                for neurone in layer:
                    layer_prec_output_array = []
                    for neurone_prec in network[index-1]:
                        layer_prec_output_array.append(neurone_prec.calc_output())
                    neurone.calc_sigma(layer_prec_output_array)
        # Output neuron error
        network[-1][0].calc_output()
        if network[-1][0].output > 0.25:
            network[-1][0].output = 1
        else:
            network[-1][0].output = 0
        network[-1][0].error = wanted_value - network[-1][0].output
        if network[-1][0].error == 0:
            correct_guess += 1
        else:
            # Back propagation
            for index1 in reversed(range(len(network))):
                # Couche de sortie
                if index1 == len(network) - 1:
                    for neurone in network[index1]:
                        for index2 in range(len(neurone.poids)):
                            neurone.poids[index2] -= coef_appr * neurone.error * neurone.sigma
                elif index1 == 0:
                    error_sigma = 0
                    for j in range(len(network[index1])):
                        for k in range(len(network[index1+1])):
                            error_sigma += network[index1+1][k].error * network[index1+1][k].poids[j]
                        network[index1][j].error = network[index1][j].calc_error(error_sigma)
                        for k in range(len(network[index1][j].poids)):
                            network[index1][j].poids[k] += coef_appr * network[index1][j].error * normalised.iloc[iterator1][k]
                else:
                    error_sigma = 0
                    for j in range(len(network[index1])):
                        for k in range(len(network[index1+1])):
                            error_sigma += network[index1+1][k].error * network[index1+1][k].poids[j]
                        network[index1][j].error = network[index1][j].calc_error(error_sigma)
                        for k in range(len(network[index1][j].poids)):
                            network[index1][j].poids[k] += coef_appr * network[index1][j].error * network[index1-1][k].output


    print("-------------------------------------------------")
    print(correct_guess * 100 / learn_range)
    print("-------------------------------------------------")
    # Si plus de 85% des exemples sont trouvés le réseau est entrainé
    if correct_guess * 100 / learn_range > 85:
        training_complete = True

print("C'est beau")