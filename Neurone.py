import numpy as np


class Neurone:

    def __init__(self, inputs_number):
        self.poids = 0.01 * np.random.randn(inputs_number)
        self.sigma = 0
        self.output = 0
        self.error = 0

    def __str__(self):
        return f"Poids: {str(self.poids)} - Sigma: {str(self.sigma)}" \
               f" Output: {self.output} - Error: {self.error}"

    def calc_sigma(self, entrees):
        self.sigma = 0
        for index, entree in enumerate(entrees):
            self.sigma += entrees[index] * self.poids[index]

    def calc_output(self):
        self.output = 1.0 / (1.0 + np.exp(-1 * self.sigma))
        return self.output

    def calc_error(self, error_sigma):
        return (np.exp(self.sigma) / (1 + np.exp(self.sigma)) ** 2) * error_sigma
